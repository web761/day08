<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style_create.css">
</head>

<body>
    <div class="main">

        <div class="wrapper">

            <form action="" method="post" enctype="multipart/form-data"> 
                <div class="form-group">
                    <label class="form-label">Khoa</label></label>
                    <div class="form-input">
                        <select name="falcuty" id="falcuties">
                            <?php
                            $falcuty = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                            foreach ($falcuty as $key => $value) {
                                 echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                            ?>
                        </select>
                        <script>
                            var selectKhoa = document.getElementById("falcuties");
                            selectKhoa.value = "<?php echo $_POST['falcuty'] ?>"
                        </script>

                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Từ khóa</label>
                    <div class="form-input">
                        <input class="name" type="text" name="postKeyword" type="text" id="keyword" value="<?php echo isset($_POST['postKeyword']) ? $_POST['postKeyword'] : ''  ?>">    
                    </div>
                </div>

                <div class="button">

                <button class="submit-btn" type="submit" id="delete">Xóa</button>
                <button class="submit-btn" type="submit">Tìm kiếm</button>
                </div>
                <script>
                    var btnDel = document.getElementById("delete");
                    var selectKhoa = document.getElementById("falcuties");
                    var keyWord = document.getElementById("keyword");
                    btnDel.onclick = function() {
                        selectKhoa.value = "";
                        keyWord.value = "";
                    };
                </script>



            </form>
        </div>
        <div class="count">
            <div class="student-found">
                <p>Số sinh viên tìm thấy: XXX</p>
            </div>
            <div class="btn">
                <a href="signup.php" class="addBtn">Thêm</a>
            </div>
        </div>
        <div class="student-list">
            <table class="student-table">
                <tr class="header">
                    <th style="width:15%">No.</th>
                    <th style="width:30%">Tên sinh viên</th>
                    <th style="width:50%">Khoa</th>
                    <th style="text-align:center;width:10%">Action</th>
                </tr>
                <tr class="student-item">
                    <td>1</td>
                    <td style="">Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>2</td>
                    <td style="">Trần Thị B</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
   


    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>
</body>

</html>